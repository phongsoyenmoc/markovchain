﻿[System.Serializable]
public class EnhanceData
{
	public int star;
	public int probility_success;
	public int probility_fail_keep;
	public int probility_fail;
	public int probility_boom;

	public EnhanceData (int star, int probility_success, int probility_fail_keep, int probility_fail, int probility_boom)
	{
		this.star = star;
		this.probility_success = probility_success;
		this.probility_fail_keep = probility_fail_keep;
		this.probility_fail = probility_fail;
		this.probility_boom = probility_boom;
	}
}
