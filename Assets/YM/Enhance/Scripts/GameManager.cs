﻿
using Markov;
using System.Linq;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
	public string DataBlessing;
	public string DataNormal;
	public bool IsBlessing;

	public EnhanceData[] Blessing;
	public EnhanceData[] Normal;

	private MarkovChain<string> chain;
	private int currentStar;

	public const string SUCCESS = "success";
	public const string FAIL = "fail";
	public const string FAIL_KEEP = "fail_keep";
	public const string BOOM = "boom";

	private string currentState = SUCCESS;
	private int twiceFail;

	private void Start ()
	{
		Blessing = JsonHelper.FromJson<EnhanceData> (JsonHelper.FixJson (DataBlessing));
		Normal = JsonHelper.FromJson<EnhanceData> (JsonHelper.FixJson (DataNormal));
	}

	public void Enhance ()
	{
		for (int j = 0; j < 100; j++)
		{
			currentStar = 0;
			for (int i = 0; i < 1000; i++)
			{
				Request (currentStar);
				if (currentStar == 15)
				{
					Debug.Log (i);
					break;
				}
			}
		}
		//Request (currentStar);
	}

	public void Request (int star)
	{
		EnhanceData enhance = null;
		if (IsBlessing)
		{
			if (star >= Blessing.Length)
			{
				return;
			}
			enhance = Blessing[star];
		}
		else
		{
			if (star >= Normal.Length)
			{
				return;
			}
			enhance = Normal[star];
		}

		chain = new MarkovChain<string> (1);
		chain.Add (new[] { SUCCESS }, SUCCESS, enhance.probility_success);
		chain.Add (new[] { SUCCESS }, FAIL, enhance.probility_fail);
		chain.Add (new[] { SUCCESS }, FAIL_KEEP, enhance.probility_fail_keep);
		chain.Add (new[] { SUCCESS }, BOOM, enhance.probility_boom);

		chain.Add (new[] { FAIL }, SUCCESS, enhance.probility_success);
		chain.Add (new[] { FAIL }, FAIL, enhance.probility_fail);
		chain.Add (new[] { FAIL }, FAIL_KEEP, enhance.probility_fail_keep);
		chain.Add (new[] { FAIL }, BOOM, enhance.probility_boom);

		chain.Add (new[] { FAIL_KEEP }, SUCCESS, enhance.probility_success);
		chain.Add (new[] { FAIL_KEEP }, FAIL, enhance.probility_fail);
		chain.Add (new[] { FAIL_KEEP }, FAIL_KEEP, enhance.probility_fail_keep);
		chain.Add (new[] { FAIL_KEEP }, BOOM, enhance.probility_boom);

		chain.Add (new[] { BOOM }, SUCCESS, enhance.probility_success);
		chain.Add (new[] { BOOM }, FAIL, enhance.probility_fail);
		chain.Add (new[] { BOOM }, FAIL_KEEP, enhance.probility_fail_keep);
		chain.Add (new[] { BOOM }, BOOM, enhance.probility_boom);

		if (twiceFail == 2)
		{
			twiceFail = 0;
			currentState = SUCCESS;
			currentStar++;
			Debug.LogWarning ("FAIL TWICE!!!");
			return;
		}

		foreach (var item in chain.Chain (new[] { currentState }).Take (1))
		{
			currentState = item;
			switch (item)
			{
				case SUCCESS:
					twiceFail = 0;
					currentStar++;
					break;
				case FAIL:
					currentStar--;
					twiceFail++;
					break;
				case FAIL_KEEP:
					twiceFail++;
					break;
				case BOOM:
					twiceFail = 0;
					Debug.LogWarning ("BOOM");
					currentStar = 0;
					break;
				default:
					twiceFail = 0;
					Debug.LogError ("Out of state!");
					break;
			}
		}

		Debug.Log ("State =" + currentState + "    Current star =" + currentStar);
	}
}

